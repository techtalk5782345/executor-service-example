import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CachedThreadPoolExample {

    public static void main(String[] args) {
        ExecutorService executor = Executors.newCachedThreadPool();

        for (int i = 1; i <= 10; i++) {
            final int taskId = i;

            // Simulate varying execution times for tasks
            int sleepTime = (int) (Math.random() * 3000); // Sleep time between 0 to 3000 milliseconds

            executor.submit(() -> {
                System.out.println("Task " + taskId + " processed by Thread: " + Thread.currentThread().getName()
                                   + " with sleep time: " + sleepTime + " milliseconds");

                try {
                    // Simulate task execution time
                    Thread.sleep(sleepTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }

        executor.shutdown();
    }
}
