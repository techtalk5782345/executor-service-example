import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class SingleThreadExecutorExample {

    public static void main(String[] args) {
        ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();

        // Task 1: Sleep for 3 seconds
        singleThreadExecutor.execute(() -> {
            try {
                Thread.sleep(3000);
                System.out.println("Task 1 completed after 3 seconds.");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        // Task 2: Sleep for 5 seconds
        singleThreadExecutor.execute(() -> {
            try {
                Thread.sleep(5000);
                System.out.println("Task 2 completed after 5 seconds.");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        // Task 3: Sleep for 2 seconds
        singleThreadExecutor.execute(() -> {
            try {
                Thread.sleep(2000);
                System.out.println("Task 3 completed after 2 seconds.");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        // Shutdown the executor service
        singleThreadExecutor.shutdown();
    }
}
