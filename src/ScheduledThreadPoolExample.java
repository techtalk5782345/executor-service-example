import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduledThreadPoolExample {

    public static void main(String[] args) {
        ScheduledExecutorService scheduledExecutor = Executors.newScheduledThreadPool(3);

        // Task to be executed every 2 seconds
        scheduledExecutor.scheduleAtFixedRate(() -> {
            System.out.println("Hello, World! This is a scheduled task.");

            // Simulate some work
            // ...

        }, 0, 2, TimeUnit.SECONDS);

        // Let the program run for a while
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Shutdown the executor service
        scheduledExecutor.shutdown();
    }
}
