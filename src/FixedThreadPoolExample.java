import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FixedThreadPoolExample {

    public static void main(String[] args) {
        // Create a FixedThreadPool with 3 threads
        ExecutorService fixedThreadPool = Executors.newFixedThreadPool(3);

        // Submit tasks for execution
        for (int i = 1; i <= 5; i++) {
            final int taskNumber = i; // final variable for lambda expression
            fixedThreadPool.submit(() -> {
                // Task: Print task number and "Hello, World!"
                System.out.println("Task " + taskNumber + ": Hello, World! from Thread: " +
                                   Thread.currentThread().getName());
            });
        }

        // Shutdown the thread pool to release resources
        fixedThreadPool.shutdown();
    }
}
